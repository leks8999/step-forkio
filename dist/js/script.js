const btn = document.querySelector('.header__burger-icon')
const ul = document.querySelector('.header__menu ul')

btn.addEventListener('click', () => {
    ul.classList.toggle('active')
    btn.classList.toggle('open')
})

window.addEventListener('click', (e) => {
    if (!e.target.closest('.header__menu ul') && !e.target.closest('.header__burger-icon')) {
        ul.classList.remove('active')
        btn.classList.remove('open')
    }
})




